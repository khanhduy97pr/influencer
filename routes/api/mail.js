var nodemailer = require('nodemailer');

module.exports = function() {
    var options = {
        // emailing options
        transportOptions: {
            service: 'Gmail',
            auth: {
                user: 'user@gmail.com',
                pass: 'password'
            }
        },

        confirmMailOptions: {
            from: 'Do Not Reply <user@gmail.com>',
            subject: 'Successfully registered!',
            html: '<p>Your account has been successfully registered.</p>',
            text: 'Your account has been successfully registered.'
        },
        confirmSendMailCallback: function(err, info) {
            if (err) {
                throw err;
            } else {
                console.log(info.response);
            }
        },
        hashingFunction: null,
    };

    var transporter  = nodemailer.createTransport(options.transportOptions);

    var sendConfirmationEmail = function(email, callback) {
        var mailOptions = JSON.parse(JSON.stringify(options.confirmMailOptions));
        mailOptions.to = email;
        if (!callback) {
            callback = options.confirmSendMailCallback;
        }
        transporter.sendMail(mailOptions, callback);
    };
};