import re

KEY_INDEX = 0
VALUE_INDEX = 1

class StringHelper(object):
    def mostUsedWords(paragraph: str, nWords: int) -> list:
        words = re.findall(r'\w+', paragraph)
        wordCounts = {}
        for word in words:
            wordLower = word.lower()
            if wordLower not in wordCounts:
                wordCounts[wordLower] = 1
            else:
                wordCounts[wordLower] += 1
        wordCountsSorted = sorted(wordCounts.items(), key=lambda x: (-x[VALUE_INDEX], x[KEY_INDEX]))
        return wordCountsSorted[:nWords]
