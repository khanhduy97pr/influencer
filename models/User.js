var mongoose = require('mongoose');
var crypto = require('crypto');

var UserSchema = new mongoose.Schema({
  userID: {type: String, index: true},
  email: {type: String, index: true},
  name: String,
  address: String, 
  phoneNumber: String,
  hash: String,
  salt: String
}, {timestamps: true});

UserSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.toJSON = function(){
    return {
      username: this.username,
      email: this.email,
    };
};