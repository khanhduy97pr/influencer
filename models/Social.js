var mongoose = require('mongoose');

var SocialSchema = new mongoose.Schema({
    socialID: {type: String, index: true},
    socialURL: String,
    socialType: String
  }, {timestamps: true});

SocialSchema.methods.setSocialType - function(){

}

SocialSchema.methods.toJSON = function(){
    return {
      socialID: this.socialID,
      socialURL: this.socialURL,
      socialType: this.socialType,
    };
};