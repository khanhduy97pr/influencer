var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  postID: {type: String, index: true},
  socialID: String,
  categoryID: String,
  URL: String, 
  content: String,
  share: int,
  like: int,
  comment: int
}, {timestamps: true});
